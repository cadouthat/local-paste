class PasteClient {
  constructor() {
    this.onUpdate = () => undefined;
    this.version = -1;
    this.latestVersion = -1;
    this.suspended = false;
  }

  async forceRefresh() {
    const response = await $.ajax({
      url: '/paste',
      method: 'GET',
      dataType: 'json',
    });

    this.processResponse_(response);
  }

  async clearAll() {
    this.suspend_();

    try {
      const response = await $.ajax({
        url: '/clear',
        method: 'POST',
        data: JSON.stringify({}),
        contentType: 'application/json',
        dataType: 'json',
      });

      this.processResponse_(response);
    } finally {
      this.resume_();
    }
  }

  async create(content) {
    this.suspend_();

    try {
      const newPaste = { content };

      const response = await $.ajax({
        url: '/paste',
        method: 'POST',
        data: JSON.stringify(newPaste),
        contentType: 'application/json',
        dataType: 'json',
      });

      this.processResponse_(response);
    } finally {
      this.resume_();
    }
  }

  async createFile(file) {
    this.suspend_();

    try {
      const formData = new FormData();
      formData.append('file', file);

      const response = await $.ajax({
        url: '/pasteFile',
        method: 'POST',
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
      });

      this.processResponse_(response);
    } finally {
      this.resume_();
    }
  }

  async begin(onUpdate) {
    this.onUpdate = onUpdate;

    await this.forceRefresh();

    const connect = () => {
      let retrying = false;
      const retry = () => {
        if (retrying) {
          return;
        }
        retrying = true;
        setTimeout(() => connect(), 5000);
      };

      const isSecure = window.location.protocol.startsWith('https');
      const ws = new WebSocket(`${isSecure ? 'wss' : 'ws'}://${window.location.host}/waitForUpdates`);

      ws.onerror = (err) => {
        console.error('websocket error', err);
        retry();
      };

      ws.onclose = () => {
        retry();
      };

      ws.onmessage = (event) => {
        this.latestVersion = Number(event.data);
        if (!this.suspended && this.latestVersion > this.version) {
          this.forceRefresh();
        }
      };
    };

    connect();
  }

  processResponse_(response) {
    this.version = Number(response.version);

    this.onUpdate(response.pastes);
  }

  suspend_() {
    this.suspended = true;
  }

  resume_() {
    this.suspended = false;
    if (this.latestVersion > this.version) {
      this.forceRefresh();
    }
  }
}
