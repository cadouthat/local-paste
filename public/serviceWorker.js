const VERSION = "v06";
const CACHE_NAME = `local-paste-${VERSION}`;

const PREFETCH_RESOURCES = [
  "/",
  "/manifest.json",
  "/css/style.css",
  "/js/paste-client.js",
];

const isCachable = (url) => {
  if (PREFETCH_RESOURCES.includes(url.pathname)) {
    return true;
  }

  const prefixes = ["/icons/", "/maskable-icons/"];
  if (prefixes.some((prefix) => url.pathname.startsWith(prefix))) {
    return true;
  }

  return false;
};

self.addEventListener("install", (e) => {
  e.waitUntil((async () => {
      const cache = await caches.open(CACHE_NAME);
      await cache.addAll(PREFETCH_RESOURCES);

      self.skipWaiting();
    })()
  );
});

self.addEventListener("activate", (event) => {
  event.waitUntil(
    (async () => {
      const names = await caches.keys();
      await Promise.all(
        names.map((name) => {
          if (name !== CACHE_NAME) {
            return caches.delete(name);
          }
        }),
      );
      await clients.claim();
    })(),
  );
});

const fetchWithCache = async (cache, requestOrUrl) => {
  const cached = await cache.match(requestOrUrl);
  if (cached) {
    return cached;
  }

  const fetched = await fetch(requestOrUrl);
  if (fetched.ok) {
    cache.put(requestOrUrl, fetched.clone());
  }
  return fetched;
};

const handleFetch = async (request) => {
  const cache = await caches.open(CACHE_NAME);

  if (isCachable(new URL(request.url))) {
    return fetchWithCache(cache, request);
  }

  // All other navigation routes map to the index
  if (request.mode === "navigate") {
    return fetchWithCache(cache, "/");
  }

  // Everything else falls back to the network (without caching)
  try {
    return await fetch(request);
  } catch (error) {
    return new Response("Network error", {
      status: 408,
      headers: { "Content-Type": "text/plain" },
    });
  }
};

async function createPaste(content) {
  const newPaste = { content };

  await fetch('/paste', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(newPaste),
  });
}

self.addEventListener("fetch", (event) => {
  // Requests related to Web Share Target.
  if (event.request.url.includes('/share/')) {
    event.respondWith(
      (async () => {
        try {
          const formData = await event.request.formData();
          const content = formData.get('url') || formData.get('text') || formData.get('title') || '';
          await createPaste(content);
        } catch (err) {
          console.error(err);
        }
        return Response.redirect('/', 303);
      })(),
    );
    return;
  }

  if (event.request.method !== 'GET') {
    return false;
  }
  if (event.request.referrer) {
    const referrer = new URL(event.request.referrer);
    const url = new URL(event.request.url);
    if (referrer.origin !== url.origin) {
      return false;
    }
  }

  event.respondWith(handleFetch(event.request));
});
