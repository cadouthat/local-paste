const express = require('express');
const path = require('path');
const http = require('http');
const logger = require('morgan');
const multer = require('multer');
const { Readable } = require('stream');
const { WebSocket, WebSocketServer } = require('ws');

const app = express();

app.use(logger('dev'));
app.use(express.json());

app.use(express.static(path.join(__dirname, 'public')));
app.use('/', express.static(path.join(__dirname, 'public/index.html')));

const MAX_CONTENT_STORAGE = 1024 * 1024 * 1024;
const PASTE_HISTORY_SIZE = 10;
const PASTE_TTL_MS = 1000 * 60 * 60 * 24;
const pasteList = [];
let pasteListVersion = 0;

const upload = multer({ limits: { fields: 0, files: 1, parts: 2, fileSize: MAX_CONTENT_STORAGE } });

function totalContentSize() {
  let total = 0;
  for (const paste of pasteList) {
    if (paste.file) {
      total += paste.file.length;
    } else {
      total += paste.content.length;
    }
  }
  return total;
}

function reservePaste(contentSize) {
  if (contentSize > MAX_CONTENT_STORAGE) {
    throw new Error('content exceeds max size');
  }

  while (pasteList.length >= PASTE_HISTORY_SIZE) {
    pasteList.pop();
  }

  while (totalContentSize() + contentSize > MAX_CONTENT_STORAGE) {
    pasteList.pop();
  }
}

function pasteListResponse() {
  const oldestMs = new Date().getTime() - PASTE_TTL_MS;
  const visiblePastes = pasteList
    .filter((paste) => paste.createdMs > oldestMs)
    .map((paste) => ({ ...paste, file: undefined, expiresMs: paste.createdMs + PASTE_TTL_MS }));

  return {
    pastes: visiblePastes,
    version: pasteListVersion,
  };
}

app.get('/paste', (_req, res) => {
  res.json(pasteListResponse());
});

app.get('/file', (req, res) => {
  const matches = pasteList.filter((paste) => String(paste.createdMs) == req.query.createdMs);
  if (matches.length != 1) {
    res.status(404).end();
    return;
  }

  res.status(200);
  res.set('Content-Type', 'application/octet-stream');
  Readable.from(matches[0].file).pipe(res);
});

app.post('/clear', (_req, res) => {
  pasteList.splice(0);

  onPasteStateChange();

  res.json(pasteListResponse());
});

app.post('/paste', (req, res) => {
  const content = String(req.body.content);

  reservePaste(content.length);

  pasteList.unshift({
    createdMs: new Date().getTime(),
    sourceIp: req.ip,
    content,
  });

  onPasteStateChange();

  res.json(pasteListResponse());
});

app.post('/pasteFile', upload.single('file'), (req, res) => {
  if (!req.file?.buffer?.length || !req.file?.originalname) {
    res.status(400).end();
    return;
  }

  reservePaste(req.file.buffer.length);

  pasteList.unshift({
    createdMs: new Date().getTime(),
    sourceIp: req.ip,
    file: req.file.buffer,
    fileName: req.file.originalname,
  });

  onPasteStateChange();

  res.json(pasteListResponse());
});

const server = http.createServer(app);

const webSocketServer = new WebSocketServer({ server });

webSocketServer.on('connection', (socket) => {
  socket.on('error', (err) => {
    console.error('WebSocket error', err);
  });
});

webSocketServer.on('error', (err) => {
  console.log('webSocketServer error', err);
});

function onPasteStateChange() {
  pasteListVersion++;

  webSocketServer.clients.forEach((socket) => {
    if (socket.readyState === WebSocket.OPEN) {
      socket.send(String(pasteListVersion));
    }
  });
}

module.exports = server;
